import { Container, Row, Col, Button} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import logo from './assets/TSA_Logo.png'
import Image from 'next/image'
import Styles from '../styles/Home.module.css';

export default function Home(props) {
  const {dataVideo} = props;
  console.log(dataVideo);
  return (
    <>
    {/* Title */}
    <Container className={Styles.titleContainer}>
      <Row>
        <Col xs={3}>
          <Image src={logo} width={100} height={70} />
        </Col>  
        <Col xs={9} className={Styles.titleText}>
          <span style={{color:'#004377'}}>TEMAN</span>
          &nbsp;
          <span style={{color:'#004377'}}>STARTUP</span>
          &nbsp;
          <span style={{color:'grey'}}>ACADEMY</span>
        </Col>
      </Row>
    </Container>

    {/* Content */}
    <div className={Styles.content}>
      {dataVideo.map((video) => (
        <>
        <video controls autoPlay muted name='media' width='100%'>
          <source src={video.video} type='video/mp4'/>
        </video>

        <Container>
          <Row className='mx-2'>
            <Col xs={12}><p className={Styles.subTitle}>EP : {video.id} </p></Col>
            <Col xs={12}><p className={Styles.subTitle2}>INTRODUCTION</p></Col>
            <Col xs={12}><h2>{video.videoTitle}</h2></Col>
            <Col xs={12}><p className={Styles.subTitle}>ADDITIONAL MATERIAL</p></Col>
            <Col xs={12}><p>{video.videoDescription}</p></Col>
            <Col xs={12}><p>{video.videoDescription}</p></Col>
            <Col xs={12} className='pb-4'>
            <Button 
            variant="primary" 
            size="lg" 
            className={Styles.buttonFinish}>
              FINISH THIS LESSON
            </Button>
            </Col>
          </Row>
        </Container>
        </>
      ))}
    </div>
    </>
  );
}

// Fetch
export async function getStaticProps() {
  const res = await fetch('https://61790482aa7f3400174046cc.mockapi.io/tsa/courseItem');
  const dataVideo = await res.json();
  return {
    props: {
      dataVideo,
    },
  };
}
